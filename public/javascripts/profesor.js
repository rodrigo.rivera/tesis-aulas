/**
 * index.
 * @module auditorio/index
 */
import {
    validar,
    limpiar,
    getFechas,
    insertaDatosHorario,
    auditorio_,
    tabla
} from './lib/vendor'
import bootstrap from 'bootstrap';
import '../js/datepicker/bootstrap-datepicker';
import '../js/datepicker/locales/bootstrap-datepicker.es';
//css
import '../css/datepicker/datepicker.css';
import '../css/bootstrap/bootstrap-blanco.css';
import '../js/validate/jquery.validate.js'
import '../stylesheets/style.css';
//import validate from 'jquery-validation'


let auditorios;
let auditorio;
let checkDiseno = false;

let fechaInicio = new Date();
fechaInicio.setDate(fechaInicio.getDate() - 1);
/**
 * inits boostrap 4 datepicker .
 * @doc https://github.com/uxsolutions/bootstrap-datepicker/blob/master/docs/index.rst
 * @doc https://bootstrap-datepicker.readthedocs.io/en/latest/events.html#show
 */

/**
 * elementos que se ocultos, condidicionados
 * 
 */

$('.check-diseño').hide();
$('.check-difusion').hide();
$("#otro_texto").hide();
$("#otro_difusion").hide();
$(".respuestaModal").hide();
$(".modalInicio").show();



$(".datepicker").datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    autoclose: true,
    daysOfWeekDisabled: [0, 6],
    //daysOfWeekDisabled: '0,6',
    startDate: fechaInicio,
    language: 'es',
}).on('changeDate', function onChangedDate(e) {
    $("#select-auditorio").attr("disabled", false);

});


/**
 *  Busca eventos y genera datos aleatorios 
 */
$("#fecha_buscar").click(function () {
    let $fecha = $("#fecha");
    var fecha = new Date($fecha.val());
    $('.ocultar').show();
    $('.progress').show();
    //console.log("mostrar");
    gneraHTML();
    tabla(fecha, $("#select-auditorio").val());

});

$.validator.addMethod('correoEXP', function (value) {
    console.log("entro a exprecion regular")
    return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@ciencias.unam.mx/.test(value);
}, 'El correo debe de ser el institucional');

console.log("valor de  diseño", checkDiseno);
$('#formulario').validate({
    rules: {
        nombre: {
            required: true,
            minlength: 2,
            maxlength: 40,
        },
        cargo: {
            required: true,
            minlength: 2,
            maxlength: 40,
        },
        area: {
            required: true,
            minlength: 2,
            maxlength: 40,
        },
        insitutucion: {
            required: true,
            minlength: 2,
            maxlength: 40,
        },
        telefono: {
            required: true,
            minlength: 10,
            maxlength: 10,

        },
        correo: {
            required: true,
            correoEXP: "requerido"
            //pattern: /^[A-Z]{2}[0-9]{5}$/
            //regex: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@ciencias.unam.mx/

            //email: true,
        },
        nombre_evento: {
            required: true,
            minlength: 2,
            maxlength: 250,
        },
        asistentes: {
            required: true,
            range: [1, 420]
        },
        pagina: {
            required: false,
            minlength: 7,
            maxlength: 80,
        },
        horas: {
            required: true,
            minlength: 1
        },
        serviciosNumber: {
            range: [0, 30]
        },
        resumen: { // <- NAME attribute of the input
            required: {
                depends: function () {
                    return $('#check-servicios-diseño_póster').is(':checked');
                }
            }
        },
        ponentes: { // <- NAME attribute of the input
            required: {
                depends: function () {
                    return $('#check-servicios-diseño_póster').is(':checked');
                }
            }
        },
        objetvo_del_evento: { // <- NAME attribute of the input
            required: {
                depends: function () {
                    return $('#check-servicios-diseño_póster').is(':checked');
                }
            }
        },
        departamento_o_dependencias: { // <- NAME attribute of the input
            required: {
                depends: function () {
                    return $('#check-servicios-diseño_póster').is(':checked');
                }
            }
        },
        departamento_o_dependencias: { // <- NAME attribute of the input
            required: {
                depends: function () {
                    return $('#check-servicios-diseño_póster').is(':checked');
                }
            }
        },
        tipoDiseno: {
            required: true,
            minlength: 1
        },
        "difucion-check": {
            required: {
                depends: function () {
                    return $('#Difusion').is(':checked');
                }
            },

            minlength: 1
        },
        otro_texto: {
            required: {
                depends: function () {
                    return $('#customRadio12').is(':checked');
                }
            },
            minlength: 2,
            maxlength: 50
        },

        otro_difusion: {
            required: {
                depends: function () {
                    return $('#difusion_otro').is(':checked');
                },
                minlength: 2,
                maxlength: 50
            }
        }

    },
    messages: {
        nombre: {
            required: "Nombre es requerido",
            minlength: "La longitud debe de ser mayor a 2",
            maxlength: "La longitud debe de ser menor a 41 ",
        },
        cargo: {
            required: "El Cargo es requerido",
            minlength: "La longitud debe de ser mayor a 2",
            maxlength: "La longitud debe de ser menor a 41 ",
        },
        area: {
            required: "El area de adscripcion es requerido",
            minlength: "La longitud debe de ser mayor a 2",
            maxlength: "La longitud debe de ser menor a 41 ",
        },
        insitutucion: {
            required: "La insitutución es requerida",
            minlength: "La longitud debe de ser mayor a 2",
            maxlength: "La longitud debe de ser menor a 41 ",
        },
        telefono: {
            required: "El teléfono es requerido",
            minlength: "La longitud debe de ser 10",
            maxlength: "La longitud debe de ser 10",
            number: true,
        },
        correo: {
            required: "El correo es requerido",
            minlength: "La longitud debe de ser mayor a 2",
            maxlength: "La longitud debe de ser menor a 41 ",
        },
        nombre_evento: {
            required: "Campo requerido ",
            minlength: "La longitud debe de ser mayor a 2",
            maxlength: "La longitud debe de ser menor a 250 ",
        },
        asistentes: {
            required: "Campo requerido ",
            range: "el numero de asistentes de 1 persona a 420"
        },
        pagina: {
            minlength: "La longitud debe de ser mayor a 7",
            maxlength: "La longitud debe de ser menor a 80 ",
        },
        horas: {

            required: "debes de elegir una hora ",
            minlength: "erro"
        },
        serviciosNumber: {
            range: "el rango es entre 1 y 30"
        },
        resumen: {
            required: "Este campo debe de ser obligatorio",
        },
        ponentes: {
            required: "Este campo debe de ser obligatorio",
        },
        objetvo_del_evento: {
            required: "Este campo debe de ser obligatorio",
        },
        departamento_o_dependencias: {
            required: "Este campo debe de ser obligatorio",
        },
        tipoDiseno: {
            required: "Este campo debe de ser obligatorio",
        },
        "difucion-check": {
            required: "Este campo debe de ser obligatorio",
        },
        otro_texto: {
            required: "Este campo debe de ser obligatorio",
            minlength: "La longitud debe de ser mayor a 2",
            maxlength: "La longitud debe de ser menor a 40 ",

        },
        otro_difusion: {
            required: "Este campo debe de ser obligatorio",
            minlength: "La longitud debe de ser mayor a 2",
            maxlength: "La longitud debe de ser menor a 40 ",

        }


    },
    submitHandler: function (form) {

        //form.submit();
        //console.log("exito");
        event.preventDefault();
        $(".modalFin").show();
        //$(".modal-body").append("");

        validar();
        // $(".modal-ocultar").hide();
        // $(".modal-mensaje").hide();
        // $(".modal-ocultar-aceptar").show();


    }

});

$(".guarda").click(function () {
    validar();
})

$(".cierra-modal").click(function () {
    $("#aceptarTerminosYcondiciones").prop("checked", false);
    $(".modalFin").hide();
})

$("#modalInicio").click(function(){
    $(".modalInicio").hide();
    
})

$("#aceptrarTerminos").click(function () {
    validar();
    $(".modal-ocultar-aceptar").hide();
});

/**
 * llena dinamicamente  el selector de auditorios
 */

auditorio_(function (err, data) {

    if (err) {
        console.log(err);
    }
    if (data.status == 400) {
        alert(data.mensaje);
    } else {
        //console.log($auditorio)
        //console.log(Object.keys(data[0].id));
        auditorios = data;
        data.forEach(evento => {
            $("#select-auditorio").append($('<option />', {
                value: evento._id,
                name: evento.nombre,
                text: evento.nombre + " - " + evento.descripcion,
            }));
        });
    }
});
/**
 * actuliza el el valor de el id de el auditorio 
 */
$("#select-auditorio").change(function changedSelect() {
    auditorio = $(this).val();
});

/**
 * genera html  los servicios dependiendo de el auditorio que ha seleccionado 
 * genera horario 
 * genera servicios 
 * 
 */

function gneraHTML() {
    //console.log("..." + auditorios)
    $("#fecha_buscar").attr("disabled", false);
    limpiar();

    auditorios.forEach(element => {
        if (element._id == auditorio) {
            auditorio = element;
        }
    });
    let $fecha = $("#fecha");
    let date = new Date($fecha.val());
    // Revisa si el dia que escojio es entre semana o virnes //tiene restricion por el dia 
    if (date.getDay() == 4 && auditorio.horario.viernes != undefined) {
        let h = '<div class=row> <div>';
        var iterador = 0;
        auditorio.horario.viernes.forEach(element => {
            // var h = element + '<input type="checkbox" name="horas" value="' + element + '" class= hora-' + element + '>'
            h += iterador % 2 == 0 ? '</div><div class="col-lg-2">' : '';
            h += `<div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input check-hora" value="${element}" id="${element}" name= "horas">
                        <label class="custom-control-label" for="${element}" >${element} horas</label>
                    </div>`;
            iterador++;
            if (auditorio.horario.viernes.length == iterador) {
                h += '</div> </div>';
                //console.log("-------------" + h)
                $("#horas").append(h);
            }
        });
    } else { // si es entre semana 
        let h = '<div class=row> <div>';
        var iterador = 0;
        if (auditorio.horario.entreSemana != undefined) {

            auditorio.horario.entreSemana.forEach(element => {
                // var h = element + '<input type="checkbox" name="horas" value="' + element + '" class= hora-' + element + '>'
                //console.log(i + "-" + h)
                h += iterador % 2 == 0 ? '</div><div class="col-lg-2">' : '';
                h += `<div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input check-hora" value="${element}" id="${element}" name= "horas">
                            <label class="custom-control-label" for="${element}" >${element} horas</label>
                        </div>`;
                iterador++;
                if (auditorio.horario.entreSemana.length == iterador) {
                    h += '</div> </div>';
                    //console.log("-------------" + h)
                    $("#horas").append(h);
                }
            });

        }
    }
    /**
     * acomoda los servicios dinamicos en columnas 
     */
    let codeNumber = '<div class=row> <div>';
    var iteradorCantidad = 0;
    console.log("servico cantidad ", auditorio.servicios);

    if (auditorio.servicios != undefined && auditorio.servicios.cantidad != undefined) {

        auditorio.servicios.cantidad.forEach(element => {
            // var input = '<br> ' + element + ':<br> <input type="" name="services" value=0 id=' + element + '>';        
            codeNumber += iteradorCantidad % 2 == 0 ? '</div><div class="col-lg-6">' : '';
            codeNumber += `<div class="form-group">
            <a  data-toggle="tooltip" data-placement="right" title="Favor de indicar con número ordinal">
            <label class="col-form-label" for="input-servicios-cantidad-${element.toString().toLowerCase()}">${element.replace("_", " ")}</label>
            <img src="public/images/exclamacion1.svg"  width="20" height="20">
            </a>            
                <input id="${element.toString().toLowerCase()}" name="serviciosNumber" type="number" class="form-control input-servicios-cantidad" placeholder="" autocomplete="off">
            
                        </div>`
            iteradorCantidad++;
            if (auditorio.servicios.cantidad.length == iteradorCantidad) {
                codeNumber += '</div> </div>';
                $("#serviciosNumber").append(codeNumber);
            }

        });

    }

    //console.log("servicios check ", auditorio.servicios.check)
    if (auditorio.servicios != undefined && auditorio.servicios.check != undefined) {
        let codeCheck = '<div class=row> <div>';
        var iteradorCheck = 0;
        auditorio.servicios.check.forEach(element => {
            // var input = '<input type="checkbox" name="servicios" value =' + element + ' > ' + element + ' <br>';
            codeCheck += iteradorCheck % 2 == 0 ? '</div><div class="col-lg-3">' : '';
            codeCheck += `<div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input check-servicios" value="${element}" id="check-servicios-${element.toString().toLowerCase()}">
                            <label class="custom-control-label" for="check-servicios-${element.toString().toLowerCase()}">${element.replace("_", " ").replace("_", " ")}</label>
                        </div>`;
            iteradorCheck++;
            if (auditorio.servicios.check.length == iteradorCheck) {
                codeCheck += '</div> </div>';
                $("#serviciosCheck").append(codeCheck);
            }
            
        });


    }
    const guardar = $('#validar');
    guardar.attr("disabled", false);

    $("#check-servicios-diseño_póster").change(function () {

        if ($('#check-servicios-diseño_póster').is(':checked')) {
            //alert("hubo cambio");
            $('.check-diseño').show();
            checkDiseno = true;
        } else {
            $('.check-diseño').hide();
            checkDiseno = false;
        }
    })
}


$("#Difusion").change(function () {

    if ($('#Difusion').is(':checked')) {

        $('.check-difusion').show();
    } else {
        $('.check-difusion').hide();
    }
})
/**
 * muestra el input cuando selecciona otro en la seccion de tipoEvento
 */
$(".radio-tipo-evento").change(function () {
    console.log("cambio el otro", $('#customRadio12').is(':checked'));
    if ($('#customRadio12').is(':checked')) {

        $('#otro_texto').show();
    } else {
        $('#otro_texto').hide();
    }
});

/**
 * muestra el input cuando selecciona otro en la seccion de difusion 
 */

$("#difusion_otro").change(function () {
    console.log("cambio el otro", $('#difusion_otro').is(':checked'));
    if ($('#difusion_otro').is(':checked')) {

        $('#otro_difusion').show();
    } else {
        $('#otro_difusion').hide();
    }
});


/**
 * habilita el boton de aceptar p
 */

$("#aceptarTerminosYcondiciones").change(function () {
    console.log("cambio el otro", $('#aceptarTerminosYcondiciones').is(':checked'));
    if ($('#aceptarTerminosYcondiciones').is(':checked')) {
        $(".guarda").prop('disabled', false);

        // habilito boton 
    } else {
        $(".guarda").prop('disabled', true);

    }
});
/**
 * modal 
 */

$(".respuestaModal").click(function () {
    location.reload();

})