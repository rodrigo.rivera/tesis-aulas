let DBManager = require('../modules/DBManager.js');
const { enviar } = require('../modules/Mail.js');
const { generarHTML, asunto } = require('../config/correo.js');



/**
 * Obtiene todos los auditorios 
 */
function getAuditorios(callback) {
    const firm = "[Controlador:generales::getAuditorios] ";
    DBManager.getAuditorioAll(function (err, data) {
        if (err) {
            console.log(firm + "erro=" + JSON.stringify(data));
            callback(true, data);
        } else {
            //console.log(firm + JSON.stringify(data));
            callback(false, data);
        }
    });
}

/**
 * inserta un evento 
 * @param {*} evento json con los datos de el evento
 * @param {*} callback 
 */
function insertarEvento(evento, callback) {
    const firm = "[Controlador:generales::insertarEvento] ";
    //   console.log(firm + Object.keys(evento.servicios));
    //console.log(firm , evento)
    //console.log(firm, evento)
    let json = {};
    let datos = {
        registro: {
            nombre: evento.registro.nombre,
            fecha: new Date(evento.registro.fecha),
            status: parseInt(evento.registro.status)
        },
        datos_profesores: {
            nombre: evento.datos_profesores.nombre,
            cargo: evento.datos_profesores.cargo,
            area: evento.datos_profesores.area,
            institucion: evento.datos_profesores.institucion,
            telefono: evento.datos_profesores.telefono,
            correo: evento.datos_profesores.correo
        },
        evento_tipo: {
            tipo: evento.evento_tipo.tipo,
            nombre: evento.evento_tipo.nombre,
            categoria: evento.evento_tipo.categoria,
            texto: evento.evento_tipo.texto
        },
        evento_datos: {
            id_espacio: parseInt(evento.evento_datos.id_espacio),
            auditorio: evento.evento_datos.auditorio,
            nombre: evento.evento_datos.nombre,
            fecha: new Date(evento.evento_datos.fecha),
            hora: evento.evento_datos.hora,
            asisitentes: parseInt(evento.evento_datos.asisitentes),
            tipo: evento.evento_datos.tipo,
            pagina: evento.evento_datos.pagina,
        },
        servicios: {
            cantidad: evento.servicios.cantidad,
            check: evento.servicios.check
        },
        diseno: {
            tipo: evento.diseno.tipo,
            resumen: evento.diseno.resumen,
            nombreCompleto: evento.diseno.nombreCompleto,
            objetivoEvento: evento.diseno.objetivoEvento,
            departamento: evento.diseno.departamento,
            difusion: evento.diseno.difusion,
        }
    }
    //console.log("datos: ",datos)
    let html = generarHTML(datos, asunto[0].mensaje)

    // DBManager.insertSolicitud("solicitudes", datos, function (err, mensaje) {
    //     if (err) {
    //         json["code"] = 500;
    //         json["descripcion"] = "Error al guardar";
    //         callback(true, json);
    //     } else {
    //         json["code"] = 200;
    //         json["descripcion"] = "Se Guardo con Exito";
    //         //mail.seend(datos.datos_profesores.datos_profesores.correo)
    //         callback(false, json);
    //     }
    // });

    enviar(evento.datos_profesores.correo, html, asunto[0].asunto, function (err, respuesta) {
        if (err) {
            json["code"] = 500;
            json["descripcion"] = "Error al Enviar el correo no se guardo el Registro";
            callback(true, json);
        } else {

            //console.log(firm + "----" + JSON.stringify(datos));

            DBManager.insertSolicitud("solicitudes", datos, function (err, mensaje) {
                if (err) {
                    json["code"] = 500;
                    json["descripcion"] = "Error al guardar";
                    callback(true, json);
                } else {
                    json["code"] = 200;
                    json["descripcion"] = "Se Guardo con Exito";
                    //mail.seend(datos.datos_profesores.datos_profesores.correo)
                    callback(false, json);
                }
            });
        }
    });
}

/**
 * Obtiene un auditorio
 * @param {*} id 
 * @param {*} callback 
 */
function getAuditorio(id, callback) {
    const firm = "[Controlador::generales:getAuditorio] ";
    //console.log(firm + id);
    let query = { _id: parseInt(id) };
    DBManager.queryFind("auditorios", query, {}, function (err, data) {
        if (err) {
            callback(true, data);
        } else {
            //console.log(firm + JSON.stringify(data));
            callback(false, data);
        }
    });
}

/**
 * 
 * @param {*} id 
 * @param {*} fechaI 
 * @param {*} FechaF 
 * @param {*} status 
 * @param {*} callback   regresa un json  
 * json = {
 * id = {horas de el auditorio}
 * d1:horas
 * ....
 * d5:horas
 * }
 */
function getHorario(auditorio, fechaI, status, callback) {
    const firm = new Date() + "[Generales::getHorario ]";
    let fechaT = new Date(fechaI);
    json = {}
    //console.log(firm + auditorio + " fechaI:" + fechaT)
    //console.log(fechaT);
    //fechaT.setDate(fechaT.getDate() + 1)
    //console.log(firm + auditorio + " fechaI:" + "***" + status)
    horario(auditorio, fechaI, status, json, 0, function (err, data) {
        if (err) {
            json = {
                code: 400,
                descripcion: "erro conexion DB"
            }
            callback(true, json);
        } else {
            //console.log(firm + "--" + Object.keys(data));
            //console.log(firm + JSON.stringify(data));
            callback(false, data);
            //console.log("horario " + JSON.stringify(data))
        }
    })
}

/**
 * obtine el  hoario
 * @param {*} auditorio 
 * @param {*} fecha 
 * @param {*} status 
 */
function horario(auditorio, fecha, status, json, i, callback) {
    const firm = "[Generales::horario] ";
    if (i == 5) { // caso base 
        callback(false, json);
    } else {
        let tem = new Date(fecha);
        tem.setDate(tem.getDate() - 1);
        let temSatus = { $or: [] };
        let query = {
            $and: [{ "evento_datos.fecha": { $lte: new Date(fecha) } },
            { "evento_datos.fecha": { $gte: tem } },
            { "evento_datos.id_espacio": parseInt(auditorio) },
                //{ "registro.status": status }
            ]
        }

        for (let i = 0; i < status.length; i++) {
            let t = { "registro.status": status[i] }
            temSatus.$or.push(t);
        }
        query.$and.push(temSatus);
        //console.log(firm + JSON.stringify(query));
        DBManager.queryFind("solicitudes", query, { 'evento_datos.hora': 1 }, function (err, data) {
            if (err) {
                console.log("existio un error " + data);
                callback(true, data);
            } else {
                let fechaT = new Date(fecha);
                let array = [];
                if (data[0] === undefined) {
                    json[`d-${fechaT.getDate()}`] = null;
                } else {
                    //console.log(firm + (data[1].evento_datos.hora) + "::" + JSON.stringify(query));
                    data.forEach(element => {
                        //console.log(element.evento_datos.hora)
                        element.evento_datos.hora.forEach(element => {
                            //  console.log(element)
                            array.push(element);
                        });
                    });
                    //console.log("-------------")
                    json[`d-${fechaT.getDate()}`] = array;
                }
                fechaT.setDate(fechaT.getDate() + 1);
                horario(auditorio, fechaT, status, json, i + 1, callback); // llamada recursiva 
            }
        });
    }
}





module.exports.getAuditorio = getAuditorio;
module.exports.getAuditorios = getAuditorios;
module.exports.insertarEvento = insertarEvento;
module.exports.getHorario = getHorario;